import { Controller, Get, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices/client/client-proxy';

@Controller()
export class AppController {
  constructor(@Inject('AUTH_SERVICE') private AuthService: ClientProxy) {}

  @Get()
  async getUser() {
    return this.AuthService.send(
      {
        cmd: 'get-user',
      },
      {},
    );
  }
}
